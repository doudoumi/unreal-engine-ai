// Fill out your copyright notice in the Description page of Project Settings.


#include "MyBTTask_UpdateFleePosition.h"
#include <Runtime/AIModule/Classes/BehaviorTree/Blackboard/BlackBoardKeyAllTypes.h>

#include "AiAgentGroupManager.h"
#include "IABehaviorManager.h"
#include "Kismet/KismetMathLibrary.h"
#include "SDTUtils.h"
#include "EngineUtils.h"
#include "SoftDesignTraining.h"
#include "SDTFleeLocation.h"
#include "DrawDebugHelpers.h"



EBTNodeResult::Type UMyBTTask_UpdateFleePosition::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
    float start, end;
    int startSec, endSec;
    UGameplayStatics::GetAccurateRealTime(GetWorld(), startSec, start);
    ASDTAIController* aiController = Cast<ASDTAIController>(OwnerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Object>("SelfActor"));

    if (aiController->AtJumpSegment)    //wait for jump to finish
    {
        return EBTNodeResult::Succeeded;
    }

    bool isPlayerPoweredUp = OwnerComp.GetBlackboardComponent()->GetValue< UBlackboardKeyType_Bool>("IsPlayerPoweredUp");
    if (!isPlayerPoweredUp)
    {
        return EBTNodeResult::Failed;
    }

    //Leave chase group
    AiAgentGroupManager::GetInstance()->UnregisterAIAgent(aiController);
    
    float bestLocationScore = 0.f;
    ASDTFleeLocation* bestFleeLocation = nullptr;

    FVector playerPosition = OwnerComp.GetBlackboardComponent()->GetValue< UBlackboardKeyType_Vector>("PlayerPosition");
    FVector pawnPosition = aiController->GetPawn()->GetActorLocation();
    bool isPlayerSeen = OwnerComp.GetBlackboardComponent()->GetValue< UBlackboardKeyType_Bool>("IsPlayerSeen");



    for (TActorIterator<ASDTFleeLocation> actorIterator(GetWorld(), ASDTFleeLocation::StaticClass()); actorIterator; ++actorIterator)
    {
        ASDTFleeLocation* fleeLocation = Cast<ASDTFleeLocation>(*actorIterator);
        if (fleeLocation)
        {
            float distToFleeLocation = FVector::Dist(fleeLocation->GetActorLocation(), playerPosition);

            FVector selfToPlayer = playerPosition - pawnPosition;
            selfToPlayer.Normalize();

            FVector selfToFleeLocation = fleeLocation->GetActorLocation() - pawnPosition;
            selfToFleeLocation.Normalize();

            float fleeLocationToPlayerAngle = FMath::RadiansToDegrees(acosf(FVector::DotProduct(selfToPlayer, selfToFleeLocation)));
            float locationScore = distToFleeLocation + fleeLocationToPlayerAngle * 100.f;

            if (locationScore > bestLocationScore)
            {
                bestLocationScore = locationScore;
                bestFleeLocation = fleeLocation;
            }

            //DrawDebugString(GetWorld(), FVector(0.f, 0.f, 10.f), FString::SanitizeFloat(locationScore), fleeLocation, FColor::Red, 5.f, false);
        }
    }

    if (bestFleeLocation)
    {
        OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Vector>(OwnerComp.GetBlackboardComponent()->GetKeyID("TargetPosition"), bestFleeLocation->GetActorLocation());
        OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Int>(OwnerComp.GetBlackboardComponent()->GetKeyID("TargetType"), ASDTAIController::TargetType::Flee);

        UGameplayStatics::GetAccurateRealTime(GetWorld(), endSec, end);
        aiController->timeToUpdateFleeLocation = end - start;
        return EBTNodeResult::Succeeded;
    }

	return EBTNodeResult::Failed;
}