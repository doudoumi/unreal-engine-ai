#pragma once
#include "TargetLKPInfo.h"
#include "SDTAIController.h"
#include "CoreMinimal.h"

/**
 * 
 */
class SOFTDESIGNTRAINING_API AiAgentGroupManager
{
public:   
    static AiAgentGroupManager* GetInstance();
    static void Destroy();

    void RegisterAIAgent(ASDTAIController* aiAgent);
    void UnregisterAIAgent(ASDTAIController* aiAgent);

    FVector GetPursuitPosition(ASDTAIController* aiAgent);

    TargetLKPInfo GetLKPFromGroup(const FString& targetLabel, bool& targetFound);

    void Draw() const;

private:

    //SINGLETON
    AiAgentGroupManager();
    static AiAgentGroupManager* m_Instance;

    TArray<ASDTAIController*> m_registeredAgents;

    const float m_bypassRadius = 500;
    TArray<FVector> m_positionOffset = {FVector(0,0,0), //first goes 
                                        FVector(1,0,0), FVector(-1,0,0), FVector(0,1,0), FVector(-1,0,0),   //up down left right
                                        FVector(cos(45),sin(45),0), FVector(cos(-45),sin(-45),0),    //diagonals
                                        FVector(cos(135),sin(135),0), FVector(cos(-135),sin(-135),0)
                                        };

};
