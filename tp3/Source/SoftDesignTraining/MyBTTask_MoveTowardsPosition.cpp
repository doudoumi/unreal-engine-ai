// Fill out your copyright notice in the Description page of Project Settings.


#include "MyBTTask_MoveTowardsPosition.h"
#include <Runtime/AIModule/Classes/BehaviorTree/Blackboard/BlackBoardKeyAllTypes.h>
#include <Runtime/Engine/Classes/Kismet/GameplayStatics.h>

#include "AiAgentGroupManager.h"
#include "IABehaviorManager.h"


EBTNodeResult::Type UMyBTTask_MoveTowardsPosition::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	ASDTAIController* aiController = Cast<ASDTAIController>(OwnerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Object>("SelfActor"));

	if (aiController->AtJumpSegment)	//don't change the destination if we are in a jump
	{
		return EBTNodeResult::Succeeded;
	}

	int targetType = OwnerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Int>("TargetType");

	aiController->HasReachedTarget();
	//Check if we reached the target
	if (aiController->HasReachedTarget())
	{
		AiAgentGroupManager::GetInstance()->UnregisterAIAgent(aiController);
		switch (targetType)
		{
		case ASDTAIController::TargetType::Flee:	
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::White, FString::Printf(TEXT("Reached flee destination")));
			break;
		case ASDTAIController::TargetType::Player:
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Purple, FString::Printf(TEXT("Reached Chase destination")));
			break;
		case ASDTAIController::TargetType::Collectible:
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, FString::Printf(TEXT("Reached Collectible  destination")));
			break;
		default:
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Reached no destination, maybe stuck ?")));
			break;
		}
		OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Int>(("TargetType"), ASDTAIController::TargetType::Null);
		OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Bool>(("IsPlayerPoweredUp"), false);	//to reset
	}

	if (targetType == ASDTAIController::TargetType::Null)	//no Target
	{
		return EBTNodeResult::Succeeded;
	}
	
	//Go To Target
	FVector position = OwnerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Vector>(BlackboardKey.GetSelectedKeyID());

	aiController->MoveToLocation(position, 0.5f, false, true, false, NULL, false);
	aiController->OnMoveToTarget();	


	return EBTNodeResult::Succeeded;
}
