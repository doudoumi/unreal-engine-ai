// Fill out your copyright notice in the Description page of Project Settings.

#include "IABehaviorManager.h"
#include <Runtime/Engine/Classes/Kismet/GameplayStatics.h>
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include <Runtime/AIModule/Classes/BehaviorTree/Blackboard/BlackBoardKeyAllTypes.h>

#include "AiAgentGroupManager.h"


const float maxTimeBudget = 0.001;	//in seconds

// Sets default values
AIABehaviorManager::AIABehaviorManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	m_behaviorTreeComponent = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorTreeComponent"));
	m_blackboardComponent = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComponent"));

}

void AIABehaviorManager::BeginPlay()
{
	Super::BeginPlay();

	//add all the IAs to the queue
	TArray<AActor*> IAs;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASDTAIController::StaticClass(), IAs);
	for (AActor* iaPtr : IAs) {
		UE_LOG(LogTemp, Warning, TEXT("aiController Instanciation: %d"), iaPtr->GetUniqueID());
		ASDTAIController* iaControllerPtr = Cast<ASDTAIController>(iaPtr);
		iaControllerPtr->InitDefaultBlackBoardComponent();
		updateQueue.push(iaControllerPtr);
	}
	UE_LOG(LogTemp, Warning, TEXT("Nombre d'IAs %d"), IAs.Num());
}

// Called every frame
void AIABehaviorManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	UpdateIAs(maxTimeBudget);
    //Draw group information
    AiAgentGroupManager::GetInstance()->Draw();
}

void AIABehaviorManager::UpdateIAs(float maxTime)
{
	if (updateQueue.empty())
	{
		return;
	}
	float start, current;
	int startSec, currentSec;
	UGameplayStatics::GetAccurateRealTime(GetWorld(), startSec, start);
	UGameplayStatics::GetAccurateRealTime(GetWorld(), currentSec, current);
	float ellapsed = current - start;
	float estimateTimeOneUpdate = 0.f;
	int nbOfIAUpdated = 0;
	int nbOfTotalAi = updateQueue.size();
	while (ellapsed < (maxTime - estimateTimeOneUpdate) && nbOfIAUpdated<=nbOfTotalAi)
	{
		ASDTAIController* aiToUpdate = updateQueue.front();
		aiToUpdate->RunBehaviorTreeOnce();
		updateQueue.pop();
		updateQueue.push(aiToUpdate);
		++nbOfIAUpdated;
		UGameplayStatics::GetAccurateRealTime(GetWorld(), currentSec, current);
		ellapsed = current - start;
		estimateTimeOneUpdate = ellapsed / nbOfIAUpdated;
	}
	UE_LOG(LogTemp, Log, TEXT("time spend for updates : %f"), current-start);
	UE_LOG(LogTemp, Log, TEXT("ia updated : %d \n\n"), nbOfIAUpdated);
	//UE_LOG(LogTemp, Warning, TEXT("updated %d IA this frame"), nbOfIAUpdated);
	return;
}

