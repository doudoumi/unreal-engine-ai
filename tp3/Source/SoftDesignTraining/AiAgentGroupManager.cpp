// Fill out your copyright notice in the Description page of Project Settings.

#include "AiAgentGroupManager.h"
#include "MyBTTask_UpdatePursuitPosition.h"
#include "SoftDesignTraining.h"

AiAgentGroupManager* AiAgentGroupManager::m_Instance;

AiAgentGroupManager::AiAgentGroupManager()
{
}

AiAgentGroupManager* AiAgentGroupManager::GetInstance()
{
    if (!m_Instance)
    {
        m_Instance = new AiAgentGroupManager();
    }

    return m_Instance;
}

void AiAgentGroupManager::Destroy()
{
    delete m_Instance;
    m_Instance = nullptr;
}

FVector AiAgentGroupManager::GetPursuitPosition(ASDTAIController* aiAgent)
{
    bool targetFound;
    TargetLKPInfo mostRecentPlayerPos = GetLKPFromGroup(PLAYER_LABEL, targetFound);
    if (!targetFound)
    {
        return FVector::ZeroVector;
    }
    else
    {
        int posInGroup = m_registeredAgents.Find(aiAgent);
        if (posInGroup == -1)
        {
            return FVector::ZeroVector;
        }
        return mostRecentPlayerPos.GetLKPPos() + m_bypassRadius * m_positionOffset[posInGroup % m_positionOffset.Num()];
    }
}

void AiAgentGroupManager::RegisterAIAgent(ASDTAIController* aiAgent)
{
    for (int i = 0; i < m_registeredAgents.Num(); i++)
    {
        if (m_registeredAgents[i] == nullptr)
        {
            m_registeredAgents[i] = aiAgent;
            return;
        }
    }
    //if there was no empty spot in the list, add the agent to the list
    m_registeredAgents.Add(aiAgent);
}

void AiAgentGroupManager::UnregisterAIAgent(ASDTAIController* aiAgent)
{
    int posInGroup = m_registeredAgents.Find(aiAgent);
    if (posInGroup != -1)
    {
        m_registeredAgents[posInGroup] = nullptr;
    }
}

TargetLKPInfo AiAgentGroupManager::GetLKPFromGroup(const FString& targetLabel, bool& targetfound)
{
    int agentCount = m_registeredAgents.Num();
    TargetLKPInfo outLKPInfo = TargetLKPInfo();
    targetfound = false;

    for (int i = 0; i < agentCount; ++i)
    {
        ASDTAIController* aiAgent = m_registeredAgents[i];
        if (aiAgent)
        {
            const TargetLKPInfo& targetLKPInfo = aiAgent->GetCurrentTargetLKPInfo();
            if (targetLKPInfo.GetTargetLabel() == targetLabel)
            {
                if (targetLKPInfo.GetLastUpdatedTimeStamp() > outLKPInfo.GetLastUpdatedTimeStamp())
                {
                    targetfound = targetLKPInfo.GetLKPState() != TargetLKPInfo::ELKPState::LKPState_Invalid;
                    outLKPInfo = targetLKPInfo;
                }
            }
        }
    }

    return outLKPInfo;
}

void AiAgentGroupManager::Draw() const {
    for (const ASDTAIController* agent : m_registeredAgents) {
        if (agent != nullptr)
        {
            agent->DrawGroupFlag(FColor::Purple);
        }
    }
}