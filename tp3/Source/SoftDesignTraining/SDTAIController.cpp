// Fill out your copyright notice in the Description page of Project Settings.

#include "SDTAIController.h"
#include "SoftDesignTraining.h"
#include "SDTCollectible.h"
#include "SDTFleeLocation.h"
#include "SDTPathFollowingComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
//#include "UnrealMathUtility.h"
#include "SDTUtils.h"
#include "EngineUtils.h"

#include <Runtime/AIModule/Classes/BehaviorTree/Blackboard/BlackBoardKeyAllTypes.h>
#include "AiAgentGroupManager.h"

ASDTAIController::ASDTAIController(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer.SetDefaultSubobjectClass<USDTPathFollowingComponent>(TEXT("PathFollowingComponent")))
{
    m_behaviorTreeComponent = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorTreeComponent"));
    m_blackboardComponent = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComponent"));
}

//Indicates that player is still moving toward target
void ASDTAIController::OnMoveToTarget()
{
    m_ReachedTarget = false;
}

//Indicates that player has reach target
void ASDTAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
    Super::OnMoveCompleted(RequestID, Result);

    m_ReachedTarget = true;
}

//Interrupt stage when agent die
void ASDTAIController::AIStateInterrupted()
{
    StopMovement();
    m_ReachedTarget = true;
    AtJumpSegment = false;
   AiAgentGroupManager::GetInstance()->UnregisterAIAgent(this); 
}

//Return agent current LKPInfo
TargetLKPInfo& ASDTAIController::GetCurrentTargetLKPInfo()
{
    return m_TargetLKPInfo;
}

//Draw a flag when if agent is part of a group
void ASDTAIController::DrawGroupFlag(FColor color) const {
    DrawDebugSphere(GetWorld(), GetPawn()->GetActorLocation() + 300.0f * FVector::UpVector, 32.0f, 32, color);
    ShowNavigationPath();
}

//Display cpu usage relate to the agent
void ASDTAIController::DisplayCPUUsage()
{
    const FString str = FString::Printf(TEXT("Chase %f \n Flee %f \n Collectible %f"), 
                            timeToUpdateChaseLocation,
                            timeToUpdateFleeLocation, 
                            timeToUpdateCollectibleLocation);
    DrawDebugString(GetWorld(), FVector(0, 0, 5.0f), str, GetPawn(), FColor::Purple, 0.f, false);
}

void ASDTAIController::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
    Super::EndPlay(EndPlayReason);
    AiAgentGroupManager* aiAgentGroupManager = AiAgentGroupManager::GetInstance();
    if (aiAgentGroupManager)
    {
        aiAgentGroupManager->UnregisterAIAgent(this);
    }
}

//Run the behavior tree once for the agent
void ASDTAIController::RunBehaviorTreeOnce()
{
    float start, end;
    int startSec, endSec;
    UGameplayStatics::GetAccurateRealTime(GetWorld(), startSec, start);
    m_behaviorTreeComponent->StartTree(*m_aiBehaviorTree, EBTExecutionMode::SingleRun);
    UGameplayStatics::GetAccurateRealTime(GetWorld(), endSec, end);
    timeToUpdateChaseLocation = end - start;
}

//Initialize black board component
void ASDTAIController::InitDefaultBlackBoardComponent()
{
    m_blackboardComponent->InitializeBlackboard(*(m_aiBehaviorTree->BlackboardAsset));

    //Set this agent in the BT
    m_blackboardComponent->SetValue<UBlackboardKeyType_Object>(m_blackboardComponent->GetKeyID("SelfActor"), this);
    //default Values
    m_blackboardComponent->SetValue<UBlackboardKeyType_Bool>(m_blackboardComponent->GetKeyID("PlayerSeen"), false);
    m_blackboardComponent->SetValue<UBlackboardKeyType_Bool>(m_blackboardComponent->GetKeyID("IsPlayerPoweredUp"), false);
    m_blackboardComponent->SetValue<UBlackboardKeyType_Vector>(m_blackboardComponent->GetKeyID("TargetPosition"), this->GetPawn()->GetActorLocation());
    m_blackboardComponent->SetValue<UBlackboardKeyType_Vector>(m_blackboardComponent->GetKeyID("PlayerPosition"), FVector(0, 0, 0));
    m_blackboardComponent->SetValue<UBlackboardKeyType_Enum>(m_blackboardComponent->GetKeyID("TargetType"), ASDTAIController::TargetType::Null);
}

void ASDTAIController::ShowNavigationPath() const
{
       if (UPathFollowingComponent* pathFollowingComponent = GetPathFollowingComponent())
           {
                   if (pathFollowingComponent->HasValidPath())
                       {
                               const FNavPathSharedPtr path = pathFollowingComponent->GetPath();
                               TArray<FNavPathPoint> pathPoints = path->GetPathPoints();
                   
                               for (int i = 0; i < pathPoints.Num(); ++i)
                                   {
                                           DrawDebugSphere(GetWorld(), pathPoints[i].Location, 10.f, 8, FColor::Yellow);
                           
                                           if (i != 0)
                                               {
                                                       DrawDebugLine(GetWorld(), pathPoints[i].Location, pathPoints[i - 1].Location, FColor::Yellow);
                                                   }
                                       }
                           }
               }
   }
