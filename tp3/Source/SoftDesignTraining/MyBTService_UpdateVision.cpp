// Fill out your copyright notice in the Description page of Project Settings.

#include "MyBTService_UpdateVision.h"
#include <Runtime/AIModule/Classes/BehaviorTree/Blackboard/BlackBoardKeyAllTypes.h>
#include "SoftDesignTraining.h"
#include "SDTAIController.h"
#include "SDTUtils.h"
#include <SoftDesignTraining/SDTUtils.h>

UMyBTService_UpdateVision::UMyBTService_UpdateVision(const FObjectInitializer& ObjectInitializer)
    :Super(ObjectInitializer) {
    NodeName = "Update Vision";
}

void UMyBTService_UpdateVision::OnSearchStart(struct FBehaviorTreeSearchData& SearchData) {
    Super::OnSearchStart(SearchData);

    ASDTAIController* aiController = Cast<ASDTAIController>(SearchData.OwnerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Object>("SelfActor"));

    //pour debug
    //UE_LOG(LogTemp, Log, TEXT("aiController: %d" ), aiController->GetUniqueID());
    //GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Magenta, FString::Printf(TEXT(" Update Vision")));

    APawn* selfPawn = aiController->GetPawn();
    if (!selfPawn)
        return;

    //si pas de joueur
    ACharacter* playerCharacter = UGameplayStatics::GetPlayerCharacter(selfPawn->GetWorld(), 0);
    if (!playerCharacter)
    {
        SearchData.OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Bool>(SearchData.OwnerComp.GetBlackboardComponent()->GetKeyID("PlayerSeen"), false);
        SearchData.OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Bool>(SearchData.OwnerComp.GetBlackboardComponent()->GetKeyID("IsPlayerPoweredUp"), false);
        return;
    }

    //update Blackboard values with capsule cast
    FVector detectionStartLocation = selfPawn->GetActorLocation() + selfPawn->GetActorForwardVector() * aiController->m_DetectionCapsuleForwardStartingOffset;
    FVector detectionEndLocation = detectionStartLocation + selfPawn->GetActorForwardVector() * aiController->m_DetectionCapsuleHalfLength * 2;

    TArray<TEnumAsByte<EObjectTypeQuery>> detectionTraceObjectTypes;
    detectionTraceObjectTypes.Add(UEngineTypes::ConvertToObjectType(COLLISION_PLAYER));

    TArray<FHitResult> allDetectionHits;
    selfPawn->GetWorld()->SweepMultiByObjectType(allDetectionHits, detectionStartLocation, detectionEndLocation, FQuat::Identity, detectionTraceObjectTypes, FCollisionShape::MakeSphere(aiController->m_DetectionCapsuleRadius));

    //UE_LOG(LogTemp, Log, TEXT("number of detection hit %d"), allDetectionHits.Num());

    for (const FHitResult& hit : allDetectionHits)
    {
        if (UPrimitiveComponent* component = hit.GetComponent())
        {
            if (component->GetCollisionObjectType() == COLLISION_PLAYER)
            {
                if(HasLoSOnHit(hit, selfPawn))
                //we can't get more important than the player
                SearchData.OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Bool>(SearchData.OwnerComp.GetBlackboardComponent()->GetKeyID("PlayerSeen"), true);
                SearchData.OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Vector>(SearchData.OwnerComp.GetBlackboardComponent()->GetKeyID("PlayerPosition"), hit.GetActor()->GetActorLocation());
                if (SDTUtils::IsPlayerPoweredUp(selfPawn->GetWorld()))
                {
                    SearchData.OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Bool>(SearchData.OwnerComp.GetBlackboardComponent()->GetKeyID("IsPlayerPoweredUp"), true);
                }
                return;
            }
        }
    }
    SearchData.OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Bool>(SearchData.OwnerComp.GetBlackboardComponent()->GetKeyID("PlayerSeen"), false);
}


void UMyBTService_UpdateVision::TickNode(UBehaviorTreeComponent* OwnerComp, uint8* NodeMemory, float DeltaSeconds) {
    Super::TickNode(*OwnerComp, NodeMemory, DeltaSeconds);
    GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Magenta, FString::Printf(TEXT("tick interval: %f delta: %f"), Interval, DeltaSeconds));
}


bool UMyBTService_UpdateVision::HasLoSOnHit(const FHitResult& hit, const AActor* pawn)
{
    if (!hit.GetComponent())
        return false;

    TArray<TEnumAsByte<EObjectTypeQuery>> TraceObjectTypes;
    TraceObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldStatic));

    FVector hitDirection = hit.ImpactPoint - hit.TraceStart;
    hitDirection.Normalize();

    FHitResult losHit;
    FCollisionQueryParams queryParams = FCollisionQueryParams();
    queryParams.AddIgnoredActor(hit.GetActor());
    
    pawn->GetWorld()->LineTraceSingleByObjectType(losHit, hit.TraceStart, hit.ImpactPoint + hitDirection, TraceObjectTypes, queryParams);

    return losHit.GetActor() == nullptr;
}
