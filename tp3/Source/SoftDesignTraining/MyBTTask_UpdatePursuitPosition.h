// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "MyBTTask_UpdatePursuitPosition.generated.h"

//Player label
static const FString PLAYER_LABEL = "Player";

/**
 * 
 */
UCLASS()
class SOFTDESIGNTRAINING_API UMyBTTask_UpdatePursuitPosition : public UBTTask_BlackboardBase
{
	GENERATED_BODY()



	public:
		virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	
};
