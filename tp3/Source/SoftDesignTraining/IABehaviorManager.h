// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <chrono>
#include "SDTAIController.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"

#include "IABehaviorManager.generated.h"


UCLASS()
class SOFTDESIGNTRAINING_API AIABehaviorManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AIABehaviorManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void UpdateIAs(float maxTime);


private:
	std::queue<ASDTAIController*> updateQueue;

	UPROPERTY(transient)
		UBehaviorTreeComponent* m_behaviorTreeComponent;
		UBlackboardComponent* m_blackboardComponent;

};
