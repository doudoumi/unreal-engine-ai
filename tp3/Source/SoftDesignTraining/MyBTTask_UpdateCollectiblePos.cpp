// Fill out your copyright notice in the Description page of Project Settings.


#include "MyBTTask_UpdateCollectiblePos.h"
#include <Runtime/AIModule/Classes/BehaviorTree/Blackboard/BlackBoardKeyAllTypes.h>
#include <SoftDesignTraining/SDTCollectible.h>
#include "IABehaviorManager.h"
#include "Kismet/KismetMathLibrary.h"
#include "SDTUtils.h"
#include "EngineUtils.h"
#include "SoftDesignTraining.h"


EBTNodeResult::Type UMyBTTask_UpdateCollectiblePos::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
    float start, end;
    int startSec, endSec;
    UGameplayStatics::GetAccurateRealTime(GetWorld(), startSec, start);
    ASDTAIController* aiController = Cast<ASDTAIController>(OwnerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Object>("SelfActor"));
    int targetType = OwnerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Int>(OwnerComp.GetBlackboardComponent()->GetKeyID("TargetType"));

    if (aiController->AtJumpSegment)    //wait for jump to finish
    {
        return EBTNodeResult::Succeeded;
    }

    if (targetType == ASDTAIController::TargetType::Collectible)    //the current target is a collectible we don't need to find another targetLocation
    {
        return EBTNodeResult::Succeeded;
    }
    
    float closestSqrCollectibleDistance = 18446744073709551610.f;
    ASDTCollectible* closestCollectible = nullptr;

    TArray<AActor*> foundCollectibles;
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASDTCollectible::StaticClass(), foundCollectibles);

    while (foundCollectibles.Num() != 0)
    {
        int index = FMath::RandRange(0, foundCollectibles.Num() - 1);

        ASDTCollectible* collectibleActor = Cast<ASDTCollectible>(foundCollectibles[index]);
        if (!collectibleActor) 
        {
            return EBTNodeResult::Failed;
        }

        if (!collectibleActor->IsOnCooldown())
        {
            OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Vector>(OwnerComp.GetBlackboardComponent()->GetKeyID("TargetPosition"), foundCollectibles[index]->GetActorLocation());
            OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Int>(OwnerComp.GetBlackboardComponent()->GetKeyID("TargetType"), ASDTAIController::TargetType::Collectible);
            UGameplayStatics::GetAccurateRealTime(GetWorld(), endSec, end);
            aiController->timeToUpdateCollectibleLocation = end - start;
            return EBTNodeResult::Succeeded;
        }
        else
        {
            foundCollectibles.RemoveAt(index);
        }
    }
	return EBTNodeResult::Failed;
}
