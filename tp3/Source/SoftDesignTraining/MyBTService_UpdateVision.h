// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Services/BTService_BlackboardBase.h"
#include "MyBTService_UpdateVision.generated.h"

/**
 * 
 */
UCLASS()
class SOFTDESIGNTRAINING_API UMyBTService_UpdateVision : public UBTService_BlackboardBase
{
	GENERATED_BODY()
	UMyBTService_UpdateVision(const FObjectInitializer& ObjectInitializer);

protected:
	virtual void TickNode(UBehaviorTreeComponent* OwnerComp, uint8* NodeMemory, float DeltaSeconds);

	virtual void OnSearchStart(struct FBehaviorTreeSearchData& SearchData);
private:
	bool HasLoSOnHit(const FHitResult& hit, const AActor* pawn);
};
