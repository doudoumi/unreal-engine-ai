// Fill out your copyright notice in the Description page of Project Settings.


#include "MyBTTask_UpdatePursuitPosition.h"
#include <Runtime/AIModule/Classes/BehaviorTree/Blackboard/BlackBoardKeyAllTypes.h>
#include <Runtime/Engine/Classes/Kismet/GameplayStatics.h>
#include <Runtime/NavigationSystem/Public/NavigationSystem.h>

#include "AiAgentGroupManager.h"
#include "SDTUtils.h"
#include "IABehaviorManager.h"
#include "Kismet/KismetMathLibrary.h"
#include "EngineUtils.h"
#include "SoftDesignTraining.h"

const float distToRushTowardsPlayer = 550;

EBTNodeResult::Type UMyBTTask_UpdatePursuitPosition::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{ 
    float start, end;
    int startSec, endSec;
    UGameplayStatics::GetAccurateRealTime(GetWorld(), startSec, start);
    ASDTAIController* aiController = Cast<ASDTAIController>(OwnerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Object>("SelfActor"));

    if (aiController->AtJumpSegment)    //wait for jump to finish
    {
        return EBTNodeResult::Succeeded;
    }


    bool playerIsSeen = OwnerComp.GetBlackboardComponent()->GetValue< UBlackboardKeyType_Bool>("PlayerSeen");
    int targetType = OwnerComp.GetBlackboardComponent()->GetValue< UBlackboardKeyType_Int>("TargetType");
    FVector playerPosition = OwnerComp.GetBlackboardComponent()->GetValue< UBlackboardKeyType_Vector>("PlayerPosition");

    if (playerIsSeen) {
        if(targetType != ASDTAIController::TargetType::Player)
        {
            //Current target is not the player, the agent must enter the group
            AiAgentGroupManager::GetInstance()->RegisterAIAgent(aiController);
            aiController->GetCurrentTargetLKPInfo().SetTargetLabel(PLAYER_LABEL);
        }


        //Update player position for ai group
        float currentElapsedTime = UGameplayStatics::GetRealTimeSeconds(GetWorld());
        aiController->GetCurrentTargetLKPInfo().SetLastUpdatedTimeStamp(currentElapsedTime);
        aiController->GetCurrentTargetLKPInfo().SetLKPPos(playerPosition);
        aiController->GetCurrentTargetLKPInfo().SetLKPState(TargetLKPInfo::ELKPState::LKPState_ValidByLOS);

        float distToPlayer;
        UNavigationSystemV1* navigationSystem = FNavigationSystem::GetCurrent<UNavigationSystemV1>(GetWorld());
        navigationSystem->GetPathLength(GetWorld(), aiController->GetPawn()->GetActorLocation(), playerPosition, distToPlayer);

        if (distToPlayer < distToRushTowardsPlayer) //if we are close enough, go straight to the player
        {
            OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Vector>(OwnerComp.GetBlackboardComponent()->GetKeyID("TargetPosition"), playerPosition);
        }
        else  //else go to the pursuit position to block him
        {
            AiAgentGroupManager* pursuitGroup = AiAgentGroupManager::GetInstance();
            FVector pursuitPos = pursuitGroup->GetPursuitPosition(aiController);
            OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Vector>(OwnerComp.GetBlackboardComponent()->GetKeyID("TargetPosition"), pursuitGroup->GetPursuitPosition(aiController));
        }
        OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Int>(OwnerComp.GetBlackboardComponent()->GetKeyID("TargetType"), ASDTAIController::TargetType::Player);

        UGameplayStatics::GetAccurateRealTime(GetWorld(), endSec, end);
        aiController->timeToUpdateChaseLocation = end - start;
        return EBTNodeResult::Succeeded;
    }
    else if (targetType == ASDTAIController::TargetType::Player)   //if we still have a pursuit Target Position, we keep on going there
    {
        bool isTargetFound;
        //Ask to group for player position
        AiAgentGroupManager* pursuitGroup = AiAgentGroupManager::GetInstance();
        TargetLKPInfo LkpInfo = pursuitGroup->GetLKPFromGroup(PLAYER_LABEL,isTargetFound);
        if(isTargetFound)
        {
            //Update player position for ai group
            aiController->GetCurrentTargetLKPInfo().SetLKPPos(LkpInfo.GetLKPPos());
            aiController->GetCurrentTargetLKPInfo().SetLastUpdatedTimeStamp(LkpInfo.GetLastUpdatedTimeStamp());
            aiController->GetCurrentTargetLKPInfo().SetLKPState(TargetLKPInfo::ELKPState::LKPState_Valid);


            float distToPlayer;
            UNavigationSystemV1* navigationSystem = FNavigationSystem::GetCurrent<UNavigationSystemV1>(GetWorld());
            navigationSystem->GetPathLength(GetWorld(), aiController->GetPawn()->GetActorLocation(), playerPosition, distToPlayer);
            if (distToPlayer < distToRushTowardsPlayer) //if we are close enough, go straight to the player
            {
                OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Vector>(OwnerComp.GetBlackboardComponent()->GetKeyID("TargetPosition"), playerPosition);
            }
            else  //else go to the pursuit position to block him
            {
                FVector pursuitPos = pursuitGroup->GetPursuitPosition(aiController);
                OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Vector>(OwnerComp.GetBlackboardComponent()->GetKeyID("TargetPosition"), pursuitGroup->GetPursuitPosition(aiController));
            }
        }
        UGameplayStatics::GetAccurateRealTime(GetWorld(), endSec, end);
        aiController->timeToUpdateChaseLocation = end - start;
        return EBTNodeResult::Succeeded;
    }
    else         //we don't see the player, we don't have a targetPosition for the pursuit
    {
        return EBTNodeResult::Failed;
    }
}