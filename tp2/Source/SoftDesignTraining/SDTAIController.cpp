// Fill out your copyright notice in the Description page of Project Settings.

#include "SDTAIController.h"
#include "SoftDesignTraining.h"
#include "SDTCollectible.h"
#include "SDTFleeLocation.h"
#include "SDTPathFollowingComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "SoftDesignTrainingMainCharacter.h"
//#include "UnrealMathUtility.h"
#include "SDTUtils.h"
#include "EngineUtils.h"
#include <Runtime/NavigationSystem/Public/NavigationSystem.h>


ASDTAIController::ASDTAIController(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer.SetDefaultSubobjectClass<USDTPathFollowingComponent>(TEXT("PathFollowingComponent")))
{
    UActorComponent *component = GetComponentByClass(USDTPathFollowingComponent::StaticClass());
    m_pathFollower = Cast<USDTPathFollowingComponent>(component);
}

/**
* Choose the behavior and goes to the chosen target depending on the state
* and the environnement
*/
void ASDTAIController::GoToBestTarget(float deltaTime)
{
    ChooseBehavior(deltaTime);
}

void ASDTAIController::OnMoveToTarget()
{ 
    m_ReachedTarget = false;
}

void ASDTAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
    Super::OnMoveCompleted(RequestID, Result);

    m_ReachedTarget = true;
}


/**
*   Displays the Navigation Path that the actor is currently following
*/
void ASDTAIController::ShowNavigationPath()
{

    //Show current navigation path DrawDebugLine and DrawDebugSphere
    if (m_pathFollower != nullptr) {
        // Check there is an actual path
        FNavPathSharedPtr path = m_pathFollower->GetPath();
        if (!path.IsValid()) {
            return;
        }
        TArray<FNavPathPoint> points = path->GetPathPoints();
        if (points.Num() == 0) {
            return;
        }
        DrawDebugSphere(GetWorld(), points[0].Location, 50.0f, 32, FColor::Blue);
        if (points.Num() == 1) {
            return;
        }
        for (int32 i = 0; i < points.Num() - 1; ++i) {
            DrawDebugLine(GetWorld(), points[i].Location, points[i + 1].Location, FColor::Blue);
        }
        DrawDebugSphere(GetWorld(), points[points.Num() - 1], 50.0f, 32, FColor::Blue);
    }
}

/**
* Calls the method to update the Actor state/behavior
*/
void ASDTAIController::ChooseBehavior(float deltaTime)
{
    UpdatePlayerInteraction(deltaTime);
}

/**
* Update the jump Animation depending on the z position of the actor
*/
void ASDTAIController::UpdateJump()
{
    float height = GetPawn()->GetActorLocation().Z;
	if (height > m_lastJumpHeight && height >= 256.0f) {
		AtJumpSegment = false;
		InAir = true;
	}
	else if (height < m_lastJumpHeight && height <= 220.0f) {
		AtJumpSegment = false;
		InAir = false;
		Landing = false;
	}
	else if (height < m_lastJumpHeight && height <= 256.0f) {
		Landing = true;
    }
    m_lastJumpHeight = height;
    UE_LOG(LogTemp, Warning, TEXT("Jump %f"), height);
}

/**
* Base on its surroundings, check the best move to make
* (called everyframe)
*/
void ASDTAIController::UpdatePlayerInteraction(float deltaTime)
{
    //finish jump before updating AI state
    if (AtJumpSegment||InAir || Landing) {
        UpdateJump();
        return;
    }
    
    APawn* selfPawn = GetPawn();
    if (!selfPawn)
        return;

    ACharacter* playerCharacter = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
    if (!playerCharacter)
        return;

    FVector detectionStartLocation = selfPawn->GetActorLocation() + selfPawn->GetActorForwardVector() * m_DetectionCapsuleForwardStartingOffset;
    FVector detectionEndLocation = detectionStartLocation + selfPawn->GetActorForwardVector() * m_DetectionCapsuleHalfLength * 2;

    TArray<TEnumAsByte<EObjectTypeQuery>> detectionTraceObjectTypes;
    detectionTraceObjectTypes.Add(UEngineTypes::ConvertToObjectType(COLLISION_COLLECTIBLE));
    detectionTraceObjectTypes.Add(UEngineTypes::ConvertToObjectType(COLLISION_PLAYER));

    TArray<FHitResult> allDetectionHits;
    GetWorld()->SweepMultiByObjectType(allDetectionHits, detectionStartLocation, detectionEndLocation, FQuat::Identity, detectionTraceObjectTypes, FCollisionShape::MakeSphere(m_DetectionCapsuleRadius));

	m_Target =  GetHighestPriorityTargetLocation(allDetectionHits);
    if (m_Target != FVector::ZeroVector) {
		//Set behavior based on found target
		MoveToLocation(m_Target);
    }

    // Check if has reached the LKP + draw LKP Position Sphere
    if (m_State == State::InvestigatingLKP)
    {
        FVector lkpPos = m_currentTargetLkpInfo.GetLKPPos();
        DrawDebugSphere(GetWorld(), lkpPos, 30.0f, 32, FColor::Purple);

        //the agent reached the investigation point
        if (IsTargetReached())
        {
            m_currentTargetLkpInfo.SetLKPPos(FVector::ZeroVector);
            m_currentTargetLkpInfo.SetLKPState(TargetLKPInfo::ELKPState::LKPState_Invalid);
            m_Target = FVector::ZeroVector;
        }
    }

    ShowNavigationPath();

    DrawDebugCapsule(GetWorld(), detectionStartLocation + m_DetectionCapsuleHalfLength * selfPawn->GetActorForwardVector(), m_DetectionCapsuleHalfLength, m_DetectionCapsuleRadius, selfPawn->GetActorQuat() * selfPawn->GetActorUpVector().ToOrientationQuat(), FColor::Blue);
}

/**
* Returns the best position to go to, according to the current state, the positions of flee points, collectibles, and 
* what the AI see in the capsule in front of him
* @param hits, what has been seen in the capsule in front of the AI
*/
FVector ASDTAIController::GetHighestPriorityTargetLocation(const TArray<FHitResult>& hits)
{
    //if we flee, we keep on fleeing until we reach the flee point
    if (m_State == State::AvoidingPlayer && !IsTargetReached()) {
        //Keep avoiding player until reaching FleeLocation
        return m_Target;
    }

    //check what the player sees
    for (const FHitResult& hit : hits)
    {
        if (UPrimitiveComponent* component = hit.GetComponent())
        {
            if (component->GetCollisionObjectType() == COLLISION_PLAYER)
            {
                ASoftDesignTrainingMainCharacter* player = Cast<ASoftDesignTrainingMainCharacter>(hit.Actor.Get());
                //if the target is in line of sight
                if(SDTUtils::Raycast(GetWorld(), GetPawn()->GetActorLocation(), player->GetActorLocation()))
                    continue;

                //if we should avoid him
                if (player->IsPoweredUp()) {
                    m_State = State::AvoidingPlayer;
                    m_Target = GetBestFleeLocation(player->GetActorLocation())->GetActorLocation();
                    return m_Target;
                }
                else    //we should track him
                {
                    m_State = State::TrackingPlayer;
                    //we can't get more important than the player
                    m_currentTargetLkpInfo.SetLKPPos(hit.GetActor()->GetActorLocation());
                    m_currentTargetLkpInfo.SetLKPState(TargetLKPInfo::ELKPState::LKPState_ValidByLOS);
                    m_Target = player->GetActorLocation();
                    return m_Target;
                }
            }
        }
    }

    // No Player visible 
    //Player was lost can, the agent investigate ?
    bool canAgentInvestigate = m_currentTargetLkpInfo.GetLKPState() != TargetLKPInfo::ELKPState::LKPState_Invalid;
    if (canAgentInvestigate && CheckLKPNotOnDeathFloor())
    {
        //Set Lkp and investigation
        m_currentTargetLkpInfo.SetLKPState(TargetLKPInfo::ELKPState::LKPState_Valid);
        m_State = State::InvestigatingLKP;
        m_Target = m_currentTargetLkpInfo.GetLKPPos();
        return m_Target;
    }

    //if no target, no lkp, then we look for the closest Collectible
    m_State = State::TrackingCollectible;
    m_Target = GetClosestCollectible()->GetActorLocation();
    return m_Target;
}

void ASDTAIController::AIStateInterrupted()
{
    StopMovement();
    m_ReachedTarget = true;
}

/**
* Finds and returns the Collectible AActor that is the closest to the AI Actor
* @return the pointer to the collectible Actor
*/
AActor* ASDTAIController::GetClosestCollectible()
{
    APawn* pawn = GetPawn();
    TArray<AActor*> foundColletibles;
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASDTCollectible::StaticClass(), foundColletibles);
    UNavigationSystemV1* navigationSystem = FNavigationSystem::GetCurrent<UNavigationSystemV1>(GetWorld());
    float minCost = std::numeric_limits<float>::max();
    AActor* closestCollectible = nullptr;
    for (AActor* collectible : foundColletibles) {
        if (static_cast<ASDTCollectible*>(collectible)->IsOnCooldown())
            continue;
        float cost;
        FVector collectibleLocation = collectible->GetActorLocation();
        navigationSystem->GetPathCost(GetWorld(), pawn->GetActorLocation(), collectible->GetActorLocation(), cost);
        if (cost < minCost) {
            closestCollectible = collectible;
            minCost = cost;
        }
    }
    DrawDebugSphere(GetWorld(), closestCollectible->GetActorLocation(), 100, 16, FColor::Red, false);

    return closestCollectible;
}

/**
* Finds and returns the best Flee Location depending on the AI Actor Position
* and the player position (that we need to flee from)
* @return the pointer to the FleeLocation Actor
*/
AActor* ASDTAIController::GetBestFleeLocation(FVector playerPos)
{
    APawn* pawn = GetPawn();
    TArray<AActor*> foundFleeLocations;
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASDTFleeLocation::StaticClass(), foundFleeLocations);
    UNavigationSystemV1* navigationSystem = FNavigationSystem::GetCurrent<UNavigationSystemV1>(GetWorld());
    float minCost = std::numeric_limits<float>::max();
    AActor* closestFleeLocation = nullptr;
    //find the direction in which the AI should NOT go
    FVector ai2PlayerDirection = (playerPos - pawn->GetActorLocation()).GetSafeNormal();
    for (AActor* fleeLocation : foundFleeLocations) {
        FVector ai2FleeDirection = (fleeLocation->GetActorLocation() - pawn->GetActorLocation()).GetSafeNormal();
        //Only consider fleeing points that are not in the direction of the player
        //angle between player direction and fleepoint direct > 90�
        if (FVector::DotProduct(ai2PlayerDirection, ai2FleeDirection) < 0.0f)
        {
            float cost;
            navigationSystem->GetPathCost(GetWorld(), pawn->GetActorLocation(), fleeLocation->GetActorLocation(), cost);
            if (cost < minCost) {
                closestFleeLocation = fleeLocation;
                minCost = cost;
            }
        }
    }
    DrawDebugSphere(GetWorld(), closestFleeLocation->GetActorLocation(), 200, 16, FColor::Green, false);
    return closestFleeLocation;
}

bool ASDTAIController::IsTargetReached() 
{
    return (m_Target - GetPawn()->GetActorLocation()).Size() < 200;
}

/**
* Check if the LKP is not on a deathfloor, and update the LKP to Invalid if so
* @return true if the LKP is NOT over a deathFloor, and false otherwise
*/
bool ASDTAIController::CheckLKPNotOnDeathFloor()
{
    FVector lkpos = m_currentTargetLkpInfo.GetLKPPos();
    FNavLocation navLocation;
    FVector radiusOfCheck = FVector(200.0f, 200.0f, 1.0f); // Can be adjusted to the agent not an exact value.
    FNavAgentProperties navAgentProprietes;
    navAgentProprietes.AgentHeight = 96.0f;
    navAgentProprietes.AgentRadius = 42.0f;
    navAgentProprietes.AgentStepHeight = 45.0f;
    UNavigationSystemV1* navigationSystem = FNavigationSystem::GetCurrent<UNavigationSystemV1>(GetWorld());
    return navigationSystem->ProjectPointToNavigation(lkpos, navLocation, radiusOfCheck, &navAgentProprietes);
}
