// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SDTBaseAIController.h"
#include "SDTPathFollowingComponent.h"
#include "TargetLKPInfo.h"

#include "SDTAIController.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = AI, config = Game)
class SOFTDESIGNTRAINING_API ASDTAIController : public ASDTBaseAIController
{
	GENERATED_BODY()

public:
    ASDTAIController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
    float m_DetectionCapsuleHalfLength = 500.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
    float m_DetectionCapsuleRadius = 250.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
    float m_DetectionCapsuleForwardStartingOffset = 100.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
    UCurveFloat* JumpCurve;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
    float JumpApexHeight = 300.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
    float JumpSpeed = 1.f;

    //variables for the jump animation Blueprint
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = AI)
    bool AtJumpSegment = false;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = AI)
    bool InAir = false;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = AI)
    bool Landing = false;

    /**
     * Last jump height.
     */
    float m_lastJumpHeight;

    //possible AI States in priority order
    enum State
    {
        AvoidingPlayer = 0,
        TrackingPlayer = 1,
        InvestigatingLKP = 2,
        TrackingCollectible = 3
    };

public:
    virtual void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;

    void AIStateInterrupted();

protected:
    void OnMoveToTarget();
    FVector GetHighestPriorityTargetLocation(const TArray<FHitResult>& hits);
    void UpdatePlayerInteraction(float deltaTime);
    void UpdateJump();

private:
    virtual void GoToBestTarget(float deltaTime) override;
    virtual void ChooseBehavior(float deltaTime) override;
    virtual void ShowNavigationPath() override;
    AActor* GetClosestCollectible();
    AActor* GetBestFleeLocation(FVector playerLocation);
    bool IsTargetReached();
    bool CheckLKPNotOnDeathFloor();

    FVector m_Target = FVector::ZeroVector;
    State m_State = State::TrackingCollectible; //default state, we track collectibles
    USDTPathFollowingComponent* m_pathFollower;

    //LKP 
    TargetLKPInfo m_currentTargetLkpInfo;
};
