// Fill out your copyright notice in the Description page of Project Settings.

#include "SDTPathFollowingComponent.h"
#include "SoftDesignTraining.h"
#include "SDTUtils.h"
#include "SDTAIController.h"
#include "GameFramework/CharacterMovementComponent.h"

#include "DrawDebugHelpers.h"

USDTPathFollowingComponent::USDTPathFollowingComponent(const FObjectInitializer& ObjectInitializer)
{

}

void USDTPathFollowingComponent::FollowPathSegment(float DeltaTime)
{
    if (!Path->IsValid()) {
        return;
    }
    const TArray<FNavPathPoint>& points = Path->GetPathPoints();
    const FNavPathPoint& segmentStart = points[MoveSegmentStartIndex];
    const FNavPathPoint& segmentEnd = points[MoveSegmentStartIndex + 1];

    //update jump
	AActor* parent = GetOwner();
	ASDTAIController* controller = Cast<ASDTAIController>(parent);
    if (SDTUtils::HasJumpFlag(segmentStart))
    {
       // handled by ai controller 
		UCharacterMovementComponent* mvt = reinterpret_cast<UCharacterMovementComponent*>(controller->GetPawn()->GetMovementComponent());
        mvt->DoJump(false);
    }
    else
    {
        //update navigation along path
        Super::FollowPathSegment(DeltaTime);
    }
}

void USDTPathFollowingComponent::SetMoveSegment(int32 segmentStartIndex)
{
    Super::SetMoveSegment(segmentStartIndex);

    const TArray<FNavPathPoint>& points = Path->GetPathPoints();
    const FNavPathPoint& segmentStart = points[MoveSegmentStartIndex];

	AActor* parent = GetOwner();
	ASDTAIController* controller = Cast<ASDTAIController>(parent);
    if (SDTUtils::HasJumpFlag(segmentStart) && FNavMeshNodeFlags(segmentStart.Flags).IsNavLink())
    {
        //Handle starting jump
		UCharacterMovementComponent* mvt = reinterpret_cast<UCharacterMovementComponent*>(controller->GetPawn()->GetMovementComponent());
		if (mvt == nullptr) {
			return;
		}
        controller->m_lastJumpHeight = 0.0f;
        controller->AtJumpSegment = true;
        controller->InAir = false;
        controller->Landing = false;
        mvt->DoJump(false);
    } else { // Handle normal segments
    	controller->AtJumpSegment = false;
        controller->InAir = false;
        controller->Landing = false;
    }
}

