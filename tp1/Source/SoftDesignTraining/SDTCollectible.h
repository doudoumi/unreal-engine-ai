// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "SDTCollectible.generated.h"

/**
 *
 */
UCLASS()
class SOFTDESIGNTRAINING_API ASDTCollectible : public AStaticMeshActor
{
	GENERATED_BODY()

public:
	ASDTCollectible();

	void Collect();
	void OnCooldownDone();
	bool IsOnCooldown();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		float m_CollectCooldownDuration = 10.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
		bool isMoveable = false;

	virtual void Tick(float deltaTime) override;
	virtual void BeginPlay() override;

	float UpdateSpeed(float deltaTime); //Update the speed of the collectible depending on its state

	FHitResult MoveTowardsDirection(FVector dir, float speed, float deltaTime);

	void UpdateState(); //Update the state of the collectible, using a raycast to detect walls


	FVector initialPosition;

protected:
	FTimerHandle m_CollectCooldownTimer;

	enum State {    //different states of our grey collectible state Machine
		Initial,    //initial state
		AccessRight,    //Access Right state, when the collectible is moving to the right
		AccessLeft, //Access Left state, when the collectible is moving to the left
		Cooldown    //When the collectible has been picked up and is on cooldown
	};

	State currentState;

private:
	// Constants

	/**
	 * AI agents follow an uniformly accelerated movement.
	 */
	UPROPERTY(EditAnywhere, meta = (DisplayName = "Acceleration", Tooltip = "Acceleration of the Moveable Collectible", ClampMin = "0", ClampMax = "500"), Category = "Moveable Collectible")
		float m_accel = 250.0f; //acceleration of the grey collectible
	UPROPERTY(EditAnywhere, meta = (DisplayName = "Maximum Speed", Tooltip = "Maximum speed of the Moveable Collectible", ClampMin = "0", ClampMax = "500"), Category = "Moveable Collectible")
		float m_maxSpeed = 500.0f; //maximum speed of the grey collectible
	UPROPERTY(EditAnywhere, meta = (DisplayName = "Wall Detection Distance", Tooltip = "The distance from which the collectible can detect a wall and start turning back", ClampMin = "0", ClampMax = "2000"), Category = "Moveable Collectible")
		float m_detectDistance = 600.0f;    //detection distance of the walls

		// Fields
	float m_speed = 0.0f;   //current speed of the collectible



};
