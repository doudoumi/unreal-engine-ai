// Fill out your copyright notice in the Description page of Project Settings.

#include "SDTAIController.h"
#include "SoftDesignTraining.h"

#include "SoftDesignTrainingMainCharacter.h"

#include "DrawDebugHelpers.h"

#include "PhysicsHelpers.h"
#include "SDTCollectible.h"

void ASDTAIController::Tick(float deltaTime)
{
	float speed = UpdateSpeed(m_accel, deltaTime);
	APawn* pawn = GetPawn();
	const FVector location = pawn->GetActorLocation();
	PhysicsHelpers physicsHelper(GetWorld());

	TArray<FOverlapResult> dynamicHits;
	DetectDynamicNearbyObjects(physicsHelper, location, dynamicHits);

	if (m_state == State::Forward) {

		MoveForward(speed, deltaTime);
		if (IsWallDetected(location, physicsHelper) || IsTrapDetected(location, physicsHelper)) {
			Avoid(location, physicsHelper);
		}

		FVector pickupLocation;
		if (IsCollectibleDetected(physicsHelper, location, dynamicHits, pickupLocation)) {
			MoveTowardsLocation(location, speed, deltaTime, pickupLocation);
		}

		if (m_isAvoidingPlayer) {
			m_isAvoidingPlayer = IsPlayerFollowing(physicsHelper, location);
		}

		FVector playerLocation;
		if (IsPlayerDetected(physicsHelper, location, dynamicHits, playerLocation)) {
			ReactToPlayer(location, playerLocation);
		}
	}
	else if (m_state == State::Tracking) {
		//Check if the player is still detected
		FVector playerLocation;
		if (IsPlayerDetected(physicsHelper, location, dynamicHits, playerLocation)) {
			MoveTowardsLocation(location, speed, deltaTime, playerLocation);
			ReactToPlayer(location, playerLocation);
		}
		else {
			//Player is not visible anymore
			m_state = State::Forward;
		}
	}
	else if (m_state == State::Avoiding) {
		m_isAvoidingPlayer = true;
		m_state = State::Forward;
	}
	else if (m_state == State::Rotating) {
		Rotate();
	}

}

float ASDTAIController::UpdateSpeed(float accel, float deltaTime) {
	float nextSpeed = m_speed + accel * deltaTime;
	if (nextSpeed >= m_maxSpeed) {
		nextSpeed = m_maxSpeed;
	}
	m_speed = nextSpeed;
	return nextSpeed;
}

void ASDTAIController::Rotate() {

	m_rotatingFactor += m_isAvoidingPlayer ? m_avoidingRotatingSpeed : m_rotatingSpeed;
	m_direction = (1.0f - m_rotatingFactor) * m_rotatingSource + m_rotatingFactor * m_rotatingTarget;
	GetPawn()->SetActorRotation(m_direction.ToOrientationQuat());
	if (m_rotatingFactor >= 1.0f) {
		m_direction = m_rotatingTarget;
		m_state = State::Forward;
	}
}

void ASDTAIController::DetectDynamicNearbyObjects(PhysicsHelpers& physicsHelper, FVector location, TArray<FOverlapResult>& dynamicHits) {
	FVector sphereOffset = m_direction * m_trackRadius;
	FVector sphereLocation = location + sphereOffset;
	physicsHelper.SphereOverlap(sphereLocation, m_trackRadius, dynamicHits, false);
}

bool ASDTAIController::IsWallDetected(FVector location, PhysicsHelpers physicsHelper) {
	TArray<FHitResult> hits;
	physicsHelper.CastRay(location, location + m_direction * m_wallDetectionDistance, hits, false);
	if (hits.Num() > 0) {
		m_obstacleNormal = hits[0].Normal;
		return true;
	}
	return false;
}

bool ASDTAIController::IsPlayerDetected(PhysicsHelpers& physicsHelper, FVector location, const TArray<FOverlapResult>& dynamicHits, FVector& playerLocation) {
	ASoftDesignTrainingMainCharacter* player = Cast<ASoftDesignTrainingMainCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	for (FOverlapResult hit : dynamicHits)
	{
		if (hit.GetActor() == player) {
			//Player is inside the detection sphere, we must check for obstacles
			playerLocation = player->GetActorLocation();
			if (!IsReachable(physicsHelper, location, playerLocation)) {
				return false;
			}

			return true;
		}
	}
	return false;
}

bool ASDTAIController::IsPlayerFollowing(PhysicsHelpers& physicsHelper, FVector location)
{
	TArray<FOverlapResult> hits;
	physicsHelper.SphereOverlap(location, m_escapeRadius, hits, false);
	ASoftDesignTrainingMainCharacter* player = Cast<ASoftDesignTrainingMainCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	for (FOverlapResult hit : hits) {
		if (hit.GetActor() == player) {
			return true;
		}
	}
	return false;
}

bool ASDTAIController::IsTrapDetected(FVector location, PhysicsHelpers physicsHelper) {
	TArray<FHitResult> deathHits;
	FVector sphereOffset = m_direction * m_trapDetectionDistance;
	sphereOffset.Z = m_trapDetectionZ;
	FVector sphereLocation = location + sphereOffset;
	physicsHelper.SphereCast(sphereLocation, sphereLocation, m_trapDetectionRadius, deathHits, false);
	if (deathHits.Num() > 0) {
		m_obstacleNormal = m_direction;
		return true;
	}
	return false;
}

bool ASDTAIController::IsCollectibleDetected(PhysicsHelpers& physicsHelper, FVector location, const TArray<FOverlapResult>& dynamicHits, FVector& pickupLocation) {
	for (FOverlapResult hit : dynamicHits) {
		AActor* actor = hit.GetActor();
		if (actor->IsA(ASDTCollectible::StaticClass())) {
			ASDTCollectible* collectible = reinterpret_cast<ASDTCollectible*>(actor);
			if (!collectible->IsOnCooldown()) {
				pickupLocation = hit.GetActor()->GetActorLocation();
				if (IsReachable(physicsHelper, location, pickupLocation)) {
					return true;
				}
			}
		}
	}
	return false;
}

void ASDTAIController::MoveForward(float speed, float deltaTime) {
	APawn* pawn = GetPawn();
	pawn->AddMovementInput(m_direction, speed * deltaTime);
	pawn->SetActorRotation(m_direction.ToOrientationQuat());
}

void ASDTAIController::Avoid(FVector location, PhysicsHelpers& physicsHelper) {
	m_speed = 0.0f;
	m_state = State::Rotating;
	m_rotatingFactor = 0.0f;
	m_rotatingSource = m_direction;
	//Add a random aspect to the movement
	float sign = FMath::Rand() % 2 == 0 ? 1.0f : -1.0f;
	m_rotatingTarget = FVector::CrossProduct(sign * FVector::UpVector, m_obstacleNormal);
	TArray<FHitResult> hits;
	physicsHelper.CastRay(location, location + m_rotatingTarget * m_wallDetectionDistance * 1.5, hits, false);
	if (hits.Num() > 0) {
		//If a wall is detected in the seleted random direction, switch direction
		m_rotatingTarget *= -1.0f;
	}
}

void ASDTAIController::MoveTowardsLocation(FVector currentLocation, float speed, float deltaTime, FVector targetLocation) {
	FVector directionVec = targetLocation - currentLocation;
	FVector normalizedDirection = FVector(0.f, 0.f, 0.f);
	float distanceToTarget = 0.f;
	directionVec.ToDirectionAndLength(normalizedDirection, distanceToTarget);
	m_direction = normalizedDirection.GetSafeNormal();

	MoveForward(speed, deltaTime);
}

void ASDTAIController::ReactToPlayer(FVector currentLocation, FVector playerLocation)
{
	ASoftDesignTrainingMainCharacter* player = Cast<ASoftDesignTrainingMainCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (player->IsPoweredUp()) {
		//Set agent direction to the opposite of playerLocation
		m_direction = (currentLocation - playerLocation).GetSafeNormal();
		m_state = State::Avoiding;
	}
	else {
		m_state = State::Tracking;
	}
}

bool ASDTAIController::IsReachable(PhysicsHelpers& physicsHelper, FVector location, FVector targetLocation) {
	TArray<FHitResult> hits;
	physicsHelper.CastRay(location, targetLocation, hits, false);
	for (FHitResult hit : hits) {
		AActor* actor = hit.GetActor();
		if (!actor->IsA(ASoftDesignTrainingMainCharacter::StaticClass()) && !actor->IsA(ASDTCollectible::StaticClass())) {
			return false;
		}
	}
	return true;
}
