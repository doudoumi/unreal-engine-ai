// Fill out your copyright notice in the Description page of Project Settings.

#include "SDTCollectible.h"
#include "SoftDesignTraining.h"
#include "PhysicsHelpers.h"

#include "SoftDesignTrainingGameMode.h"

#include "Engine/LevelScriptBlueprint.h"

ASDTCollectible::ASDTCollectible()
{
	PrimaryActorTick.bCanEverTick = true;
	currentState = State::Initial;
}

void ASDTCollectible::BeginPlay()
{
	Super::BeginPlay();
	if (isMoveable)
	{
		initialPosition = GetActorLocation(); //register initial location
	}
}

void ASDTCollectible::Collect()
{
	// Update collectible counter
	((ASoftDesignTrainingGameMode*)(GetWorld()->GetAuthGameMode()))->incrementCollectible();

	GetWorld()->GetTimerManager().SetTimer(m_CollectCooldownTimer, this, &ASDTCollectible::OnCooldownDone, m_CollectCooldownDuration, false);

	GetStaticMeshComponent()->SetVisibility(false);
	if (isMoveable) { currentState = State::Cooldown; }
}

void ASDTCollectible::OnCooldownDone()
{
	GetWorld()->GetTimerManager().ClearTimer(m_CollectCooldownTimer);

	GetStaticMeshComponent()->SetVisibility(true);

	if (isMoveable) {
		currentState = State::Initial;
		this->SetActorLocation(initialPosition);
	}
}

bool ASDTCollectible::IsOnCooldown()
{
	return m_CollectCooldownTimer.IsValid();
}

void ASDTCollectible::Tick(float deltaTime)
{
	Super::Tick(deltaTime);


	if (isMoveable) {
		UpdateState();

		float speed = UpdateSpeed(deltaTime);
		MoveTowardsDirection(FVector(0.0f, 1.0f, 0.0f), speed, deltaTime);
	}

}

float ASDTCollectible::UpdateSpeed(float deltaTime) {

	int direction = 1;

	switch (currentState) {
	case Initial:
		m_speed = m_maxSpeed;
		return m_maxSpeed;
	case AccessRight:
		break;
	case AccessLeft:
		direction = -1;
		break;
	case Cooldown:
		m_speed = m_maxSpeed;
		return 0;
	}


	float nextSpeed = m_speed + direction * m_accel * deltaTime;

	if (abs(nextSpeed) >= m_maxSpeed) {
		nextSpeed = direction * m_maxSpeed;
	}
	m_speed = nextSpeed;
	return nextSpeed;
}

FHitResult ASDTCollectible::MoveTowardsDirection(FVector direction, float speed, float deltaTime) {

	FHitResult hitResult;

	this->AddActorWorldOffset(direction * speed * deltaTime, true, &hitResult);

	return hitResult;
}

void ASDTCollectible::UpdateState() {

	PhysicsHelpers physicHelper(GetWorld());
	int dir = (currentState == AccessLeft) ? -1 : 1;

	TArray<FHitResult> rayHit;

	bool hitForward = physicHelper.CastRay(FVector(this->GetActorLocation()), FVector(this->GetActorLocation()) + dir * m_detectDistance * FVector(0, 1, 0), rayHit, false);

	switch (currentState) {
	case Initial:
		if (hitForward) {
			currentState = State::AccessLeft;
		}
		return;
	case AccessRight:
		if (hitForward) {
			currentState = State::AccessLeft;
		}
		return;
	case AccessLeft:
		if (hitForward) {
			currentState = State::AccessRight;
		}
		return;
	case Cooldown:
		return;
	}
}

