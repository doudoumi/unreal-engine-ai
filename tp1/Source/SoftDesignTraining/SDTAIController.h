// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "PhysicsHelpers.h"

#include "SDTAIController.generated.h"

/**
 * Pacman adversary AI class
 */
UCLASS(ClassGroup = AI, config = Game)
class SOFTDESIGNTRAINING_API ASDTAIController : public AAIController
{
	GENERATED_BODY()
public:
	virtual void Tick(float deltaTime) override;

	float UpdateSpeed(float accel, float deltaTime);

	void MoveForward(float speed, float deltaTime);
	void MoveTowardsLocation(FVector currentLocation, float speed, float deltaTime, FVector targetLocation);
	void Rotate();

	void Avoid(FVector location, PhysicsHelpers& physicsHelper);

	void ReactToPlayer(FVector location, FVector playerLocation);

	void DetectDynamicNearbyObjects(PhysicsHelpers& physicsHelper, FVector location, TArray<FOverlapResult>& dynamicHits);

	bool IsWallDetected(FVector location, PhysicsHelpers physicsHelper);
	bool IsTrapDetected(FVector location, PhysicsHelpers physicsHelper);

	bool IsCollectibleDetected(PhysicsHelpers& physicsHelper, FVector location, const TArray<FOverlapResult>& dynamicHits, FVector& collectibleLocation);
	bool IsPlayerDetected(PhysicsHelpers& physicsHelper, FVector location, const TArray<FOverlapResult>& dynamicHits, FVector& playerLocation);
	bool IsPlayerFollowing(PhysicsHelpers& physicsHelper, FVector location);

	bool IsReachable(PhysicsHelpers& physicsHelper, FVector location, FVector targetLocation);

	enum State {
		Forward = 0,
		Rotating,
		Tracking,
		Avoiding
	};


private:
	// Constants

	/**
	 * AI agents follow an uniformly accelerated movement.
	 */
	UPROPERTY(EditAnywhere, meta = (DisplayName = "Acceleration", Tooltip = "AI agent acceleration (cm/s^2)", ClampMin = "0", ClampMax = "1000"))
		float m_accel = 250.0f;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Max Speed", Tooltip = "AI agent maximum speed (cm/s)", ClampMin = "0", ClampMax = "1000"))
		float m_maxSpeed = 500.0f;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Rotation Speed", Tooltip = "Rotation factor per frame (0,1)", ClampMin = "0", ClampMax = "1"))
		float m_rotatingSpeed = 0.05f;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Avoiding rotation Speed", Tooltip = "Rotation factor per frame (0,1) while avoiding player", ClampMin = "0", ClampMax = "1"))
		float m_avoidingRotatingSpeed = 0.1f;

	// Fields
	State m_state = State::Forward;

	FVector m_direction = FVector(1.0f, 0.0f, 0.0f);

	FVector m_obstacleNormal = FVector(1.0f, 0.0f, 0.0f);

	float m_speed = 0.0f;

	bool m_isAvoidingPlayer = false;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Track Radius", Tooltip = "Detection radius for players and collectibles (cm)", ClampMin = "0", ClampMax = "1000"))
		float m_trackRadius = 400.0f;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Escape Radius", Tooltip = "Safe distance when running from a powered-up player (cm)", ClampMin = "0", ClampMax = "2000"))
		float m_escapeRadius = 1000.0f;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Wall Detection Distance", Tooltip = "Distance at which AI agents stop and turn before a wall (cm)", ClampMin = "0", ClampMax = "1000"))
		float m_wallDetectionDistance = 185.0f;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Trap Detection Distance", Tooltip = "Distance at which AI agents stop before a trap (cm)", ClampMin = "0", ClampMax = "1000"))
		float m_trapDetectionDistance = 80.0f;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Trap Detection Radius", Tooltip = "Radius encompassing trap platform height (cm)", ClampMin = "0", ClampMax = "1000"))
		float m_trapDetectionRadius = 40.0f;

	UPROPERTY(EditAnywhere, meta = (DisplayName = "Trap Detection Height", Tooltip = "Trap platform Z relative to the player (cm)", ClampMin = "-1000", ClampMax = "0"))
		float m_trapDetectionZ = -85.0f;

	float m_rotatingFactor = 0.0f;
	FVector m_rotatingSource;
	FVector m_rotatingTarget;
};