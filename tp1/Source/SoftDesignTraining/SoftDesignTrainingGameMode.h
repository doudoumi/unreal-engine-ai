// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "SoftDesignTrainingGameMode.generated.h"

UCLASS(minimalapi)
class ASoftDesignTrainingGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ASoftDesignTrainingGameMode();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Score)
		int deathCounter = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Score)
		int collectibleCounter = 0;

	int const getDeathCounter() { return deathCounter; }
	int const getCollectibleCounter() { return collectibleCounter; }

	void incrementDeath() { deathCounter++; }
	void incrementCollectible() { collectibleCounter++; }
	
};



